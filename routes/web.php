<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'middleware' => ['web','auth']
], function () {
    Route::get('/caja/', 'TransaccionController@index')->name('transaccion.index')->middleware('can:listar,App\Transaccion');
    Route::get('/caja/deposito', 'TransaccionController@deposito')->name('transaccion.deposito')->middleware('can:deposito,App\Transaccion');
    Route::post('/caja/depositar', 'TransaccionController@depositar')->name('transaccion.depositar')->middleware('can:deposito,App\Transaccion');
    Route::get('/caja/retiro', 'TransaccionController@retiro')->name('transaccion.retiro')->middleware('can:retiro,App\Transaccion');
    Route::post('/caja/retirar', 'TransaccionController@retirar')->name('transaccion.retirar')->middleware('can:retiro,App\Transaccion');

    Route::get('/prestamo/', 'PrestamoController@index')->name('prestamo.index')->middleware('can:listar,App\Prestamo');
    Route::get('/prestamo/nuevo', 'PrestamoController@create')->name('prestamo.nuevo')->middleware('can:prestar,App\Prestamo');
    Route::post('/prestamo', 'PrestamoController@store')->name('prestamo.prestamo')->middleware('can:prestar,App\Prestamo');


    Route::get('/usuario/', 'UserController@index')->name('user.index')->middleware('can:listar,App\User');
    Route::delete('/usuario/{id}', 'UserController@destroy')->name('user.eliminar')->middleware('can:eliminar,App\User');
});
