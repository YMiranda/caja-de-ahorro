@extends('layouts.app')
@section('title', 'Usuarios')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Usuarios</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                        <tr>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Saldo Disponible
                            </th>
                            <th>
                                Prestamos
                            </th>
                            <th>
                                Saldo Retenido
                            </th>
                            <th class="text-right">
                                Fecha de Alta
                            </th>
                        </tr></thead>
                        <tbody>
                        @forelse($usuarios as $usuario)
                        <tr>
                            <td>
                                {{ $usuario->name }}
                            </td>
                            <td>
                                $ {{ $usuario->saldo_disponible }}
                            </td>
                            <td>
                                $ {{ $usuario->saldo_prestamos }}
                            </td>
                            <td>
                                $ {{ $usuario->saldo_retenido }}
                            </td>
                            <td class="text-right">
                                {{ $usuario->created_at->isoFormat('LLLL') }}
                            </td>
                        </tr>
                        @empty
                            <td colspan="5" class="text-center">
                                Sin Usuarios Activos.
                            </td>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
