@extends('layouts.app')
@section('title', 'Historial de Transacciones')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Historial de Transacciones</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                        <tr>
                            <th>
                                #
                            </th>
                            @if (Bouncer::is(Auth::user())->an('admin'))
                            <th>
                                Cuenta
                            </th>
                            @endif
                            <th>
                                Monto
                            </th>
                            <th>
                                Tipo de Transacción
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th class="text-right">
                                Fecha
                            </th>
                        </tr></thead>
                        <tbody>
                        @forelse($transacciones as $transaccion)
                        <tr>
                            <td>
                                {{ $i++ }}
                            </td>
                            @if (Bouncer::is(Auth::user())->an('admin'))
                            <td>
                                {{ $transaccion->usuario->name }}
                            </td>
                            @endif
                            <td>
                                ${{ $transaccion->monto }}
                            </td>
                            <td>
                                {{ $transaccion->tipo->nombre }}
                            </td>
                            <td>
                                {{ $transaccion->estado_texto }}
                            </td>
                            <td class="text-right">
                                {{ $transaccion->created_at->isoFormat('LLLL') }}
                            </td>
                        </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Aportaciones Programadas</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                        <tr>
                            <th>
                                #
                            </th>
                            @if (Bouncer::is(Auth::user())->an('admin'))
                                <th>
                                    Cuenta
                                </th>
                            @endif
                            <th>
                                Monto
                            </th>
                            <th>
                                Fecha Programada
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th class="text-right">
                                Fecha de Solicitud
                            </th>
                        </tr></thead>
                        <tbody>
                        @forelse($aportaciones as $transaccion)
                            <tr>
                                <td>
                                    {{ $i++ }}
                                </td>
                                @if (Bouncer::is(Auth::user())->an('admin'))
                                    <td>
                                        {{ $transaccion->usuario->name }}
                                    </td>
                                @endif
                                <td>
                                    ${{ $transaccion->monto }}
                                </td>
                                <td>
                                    {{ $transaccion->fecha_abono->isoFormat('LLLL') }}
                                </td>
                                <td>
                                    {{ $transaccion->cobrado }}
                                </td>
                                <td class="text-right">
                                    {{ $transaccion->created_at->isoFormat('LLLL') }}
                                </td>
                            </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
