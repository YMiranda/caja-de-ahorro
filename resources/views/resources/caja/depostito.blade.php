@extends('layouts.app')
@section('title', 'Deposito')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="row justify-content-md-center" id="deposito">
        <div class="col-md-6">
            <div class="card-header">
                <h5 class="card-title">Deposito</h5>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ action('TransaccionController@depositar') }}" class="form-horizontal">
                    @csrf
                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label>Cuenta</label>
                                <input class="form-control" name="cuenta" disabled="" placeholder="Company" value="{{Auth::user()->name}}"
                                       type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label>Monto</label>
                                <input class="form-control" name="monto" placeholder="000.00" value="" type="number" required>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="programar" v-model="vue.programar" type="checkbox" :value="1">
                                        Programar Deposito
                                        <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-if="vue.programar" class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label>Fecha</label>
                                <input class="form-control" name="fecha" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" value="" type="date" required>
                            </div>
                        </div>
                    </div>

                    @if($pagar_prestamo)
                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" name="prestamos" type="checkbox" value="1">
                                        Pagar prestamos
                                        <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round">Depositar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
