@extends('layouts.app')
@section('title', 'Deposito')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card-header">
                <h5 class="card-title">Retiro</h5>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ action('TransaccionController@retirar') }}" class="form-horizontal">
                    @csrf
                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label>Cuenta</label>
                                <input class="form-control" disabled="" placeholder="Company" value="{{old('cuenta', Auth::user()->name)}}" name="cuenta" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label>Monto</label>
                                <input class="form-control" placeholder="000.00" value="{{old('monto')}}" type="number" name="monto" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round">Retirar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
