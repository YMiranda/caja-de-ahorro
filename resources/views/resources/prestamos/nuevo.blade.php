@extends('layouts.app')
@section('title', 'Solicitar Prestamo')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card-header">
                <h5 class="card-title">Solicitar Prestamo</h5>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ action('PrestamoController@store') }}" class="form-horizontal">
                    @csrf
                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label>Cuenta</label>
                                <input class="form-control" name="cuenta" disabled="" placeholder="Company" value="{{old('cuenta', Auth::user()->name)}}"
                                       type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label for="selectPlazo">Plazo</label>
                                <select class="form-control" id="selectPlazo" name="plazo" required>
                                    @foreach( $tipos_prestamos as $tipo_prestamo)
                                        <option value="{{$tipo_prestamo->id}}"> {{$tipo_prestamo->meses}} meses a {{ $tipo_prestamo->interes }}% de interes.</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row justify-content-md-center">
                        <div class="col-md-10 pr-1">
                            <div class="form-group">
                                <label>Monto</label>
                                <input class="form-control" name="monto" placeholder="000.00" value="{{old('monto')}}" type="number" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round">Solicitar Prestamo</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
