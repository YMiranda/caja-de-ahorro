@extends('layouts.app')
@section('title', 'Historial de Prestamos')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Historial de Prestamos</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                        <tr>
                            <th>
                                Monto Solicitado
                            </th>
                            <th>
                                Pago Mensual
                            </th>
                            <th>
                                Plazo
                            </th>
                            <th>
                                Estado
                            </th>
                            <th class="text-right">
                                Fecha de Solicitud
                            </th>
                        </tr></thead>
                        <tbody>
                        @forelse($prestamos as $prestamo)
                        <tr>
                            <td>
                                $ {{$prestamo->monto}}
                            </td>
                            <td>
                                $ {{$prestamo->pago_mensual}}
                            </td>
                            <td>
                                {{$prestamo->tipo->meses}} Meses
                            </td>
                            <td>
                                {{$prestamo->estado}}
                            </td>
                            <td class="text-right">
                                {{$prestamo->created_at->isoFormat('LLLL')}}
                            </td>
                        </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
