<div class="sidebar" data-color="white" data-active-color="danger">

    <div class="logo text-center">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            {{ config('app.name', 'Laravel') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ ((Request::is('*home*') || Request::is('/')) ? 'active' : '') }} ">
                <a href="{{route('home')}}">
                    <i class="nc-icon nc-bank"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            @can('listar', \App\User::class)
            <li class="{{ Request::is('*usuario*') ? 'active' : '' }} ">
                <a href="{{action('UserController@index')}}">
                    <i class="nc-icon nc-bank"></i>
                    <p>Usuarios</p>
                </a>
            </li>
            @endcan
            <li class="{{ Request::is('*caja*') ? 'active' : '' }} ">
                <a data-toggle="collapse" href="#collapceCaja">
                    <i class="nc-icon nc-money-coins"></i>
                    <p>
                        Caja
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ (Request::is('*caja*') ? 'show' : '') }} " id="collapceCaja">
                    <ul class="nav">
                        @can('listar', \App\Transaccion::class)
                        <li class="{{ Request::is('*caja') ? 'active' : '' }}">
                            <a href="{{action('TransaccionController@index')}}">
                                <span class="sidebar-normal"> Historial </span>
                            </a>
                        </li>
                        @endcan
                        @can('deposito', \App\Transaccion::class)
                        <li class="{{ Request::is('*caja/deposito*') ? 'active' : '' }}">
                            <a href="{{action('TransaccionController@deposito')}}">
                                <span class="sidebar-normal"> Deposito </span>
                            </a>
                        </li>
                        @endcan
                        @can('retiro', \App\Transaccion::class)
                        <li class="{{ Request::is('*caja/retiro*') ? 'active' : '' }}">
                            <a href="{{action('TransaccionController@retiro')}}">
                                <span class="sidebar-normal"> Retiro </span>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </div>
            </li>

            @if (Auth::user()->can('listar', \App\Prestamo::class) || Auth::user()->can('prestar', \App\Prestamo::class))
            <li class="{{ Request::is('*prestamo*') ? 'active' : '' }} ">
                <a data-toggle="collapse" href="#collapcePrestamos">
                    <i class="nc-icon nc-paper"></i>
                    <p>
                        Prestamos
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ (Request::is('*prestamo*') ? 'show' : '') }} " id="collapcePrestamos">
                    <ul class="nav">
                        @can('listar', \App\Prestamo::class)
                        <li class="{{ Request::is('*prestamo') ? 'active' : '' }}">
                            <a href="{{action('PrestamoController@index')}}">
                                <span class="sidebar-normal"> Historial </span>
                            </a>
                        </li>
                        @endcan
                        @can('prestar', \App\Prestamo::class)
                        <li class="{{ Request::is('*prestamo/nuevo*') ? 'active' : '' }}">
                            <a href="{{action('PrestamoController@create')}}">
                                <span class="sidebar-normal"> Nuevo </span>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </div>
            </li>
            @endif
        </ul>
    </div>
</div>
