<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent" color-on-scroll="150">
    <div class="container">
        <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav ml-auto">

                    @auth
                        <li class="nav-item btn-rotate dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{ url('/home') }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item"
                                   onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">
                                    Eliminar Cuenta
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                            </div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <form id="delete-form" action="{{ action('UserController@destroy', Auth::user()) }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="{{ route('login') }}"  class="nav-link"><i class="nc-icon nc-book-bookmark"></i> Login</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('register') }}"  class="nav-link"><i class="nc-icon nc-book-bookmark"></i> Register</a>
                        </li>
                    @endauth
            </ul>
        </div>
    </div>
</nav>
