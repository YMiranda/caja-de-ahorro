<!-- Core JS Files -->
<script src="{{secure_asset('js/paper/jquery-3.2.1.js')}}" type="text/javascript"></script>
<script src="{{secure_asset('js/paper/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
<script src="{{secure_asset('js/paper/popper.js')}}" type="text/javascript"></script>
<script src="{{secure_asset('js/paper/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Paper Kit Initialization snd functons -->
<script src="{{secure_asset('js/paper/paper-dashboard.js')}}"></script>
<script src="{{mix('js/app.js')}}"></script>
