<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">

    <link href="{{secure_asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{mix('css/paper-kit.css')}}" rel="stylesheet"/>
    <link href="{{secure_asset('css/nucleo-icons.css')}}" rel="stylesheet" />

</head>
<body>
@include('layouts.partials.nav')
<div class="wrapper">
    <div class="page-header" data-parallax="true" style="background-image: url('{{secure_asset('img/fabio-mangione.jpg')}}');">
        <div class="filter"></div>
        @yield('content')
    </div>
</div>
@yield('more-elements')
@include('layouts.partials.scripts')
</body>
