<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Prestamo
 *
 * @property-read mixed $estado
 * @property-read mixed $pago_mensual
 * @property-read \App\TipoPrestamo $tipo
 * @property-read \App\User $usuario
 * @mixin \Eloquent
 * @property int $id
 * @property float $monto
 * @property float $retenido
 * @property float $abono
 * @property int $pagado
 * @property int $tipo_prestamo_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereAbono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo wherePagado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereRetenido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereTipoPrestamoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prestamo whereUserId($value)
 */
class Prestamo extends Model
{

    /**
     * Atributos para assginacion masiva.
     *
     * @var array
     */
    protected $fillable = [
        'monto', 'retenido'
    ];


    /**
     * Mutador estado retorna al Atributo boleano 'pagado' en un String entendible
     * para el Usuario.
     *
     * @return string
     */
    public function getEstadoAttribute()
    {
        if ($this->pagado)
        {
            return "Pagado";
        }else{
            return "Pendiente de pago";
        }
    }

    /**
     * Retorna al Usuario asociado de un Prestamo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Retona el Tipo de Prestamo asociado a un Prestamo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo()
    {
        return $this->belongsTo(TipoPrestamo::class, 'tipo_prestamo_id');
    }

    /**
     * Mutador pago_mensual retorna retorna el calculo de los abonos mensuales de un prestamo,
     * ademas de formatear a dos decimales.
     *
     * @return string
     */
    public function getPagoMensualAttribute()
    {
        $pagos = $this->monto/$this->tipo->meses;
        return number_format((float) $pagos, 2, '.', '');
    }
}
