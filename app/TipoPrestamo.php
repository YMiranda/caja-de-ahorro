<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TipoPrestamo
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Prestamo[] $prestamos
 * @mixin \Eloquent
 * @property int $id
 * @property int $meses
 * @property float $interes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoPrestamo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoPrestamo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoPrestamo whereInteres($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoPrestamo whereMeses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoPrestamo whereUpdatedAt($value)
 */
class TipoPrestamo extends Model
{
    /**
     * Atributos para assginacion masiva.
     *
     * @var array
     */
    protected $fillable = [
        'meses', 'interes'
    ];

    /**
     * Retornna una coleccion de Prestamos asociados a un Tipo de Prestamo
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prestamos()
    {
        return $this->hasMany(Prestamo::class);
    }
}
