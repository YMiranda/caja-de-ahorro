<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Transaccion
 *
 * @property int $id
 * @property float $monto
 * @property int $tipo_transaccion_id
 * @property int $user_id
 * @property int $estado
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $estado_texto
 * @property-read \App\TipoTransaccion $tipo
 * @property-read \App\User $usuario
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaccion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaccion whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaccion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaccion whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaccion whereTipoTransaccionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaccion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaccion whereUserId($value)
 * @mixin \Eloquent
 */
class Transaccion extends Model
{

    /**
     * Atributos para assginacion masiva.
     *
     * @var array
     */
    protected $fillable = [
        'monto', 'estado'
    ];


    /**
     * Funcion retorna al Usuario asociado a la Transaccion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Funcion retorna el Tipo de Transaccion asociado a la Transaccion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo()
    {
        return $this->belongsTo(TipoTransaccion::class, 'tipo_transaccion_id');
    }

    /**
     * Mutador estado_texto retorna al Atributo boleano 'estado' en un String entendible
     * para el Usuario.
     *
     * @return string
     */
    public function getEstadoTextoAttribute()
    {
        if ($this->attributes['estado'])
        {
            return "Rechazado";
        }
        else {
            return "Aprobado";
        }
    }

    /**
     * Funcion estatica retorna el Total de Saldo Disponible en la Caja de Ahorro.
     *
     * @return mixed
     */
    public static function saldoDisponible()
    {
        $trans = (new static)::where('estado', false)->get();
        $depositos = $trans->where('tipo_transaccion_id', 1)->sum('monto');
        $retiros = $trans->where('tipo_transaccion_id', 2)->sum('monto');

        return $depositos-($retiros + (new static())::saldoRetenido());
    }

    /**
     * Funcio estatica retona el Total de Saldos Retenidos de todos los prestamos con adeudos.
     *
     * @return mixed
     */
    public static function saldoRetenido()
    {
        $prestamos = Prestamo::wherePagado(false)->get();
        return $prestamos->sum('retenido');
    }

    /**
     * Funcion estatica retorna el Total de Saldos en Prestamos con adeudos.
     *
     * @return mixed
     */
    public static function saldoPrestado()
    {
        $prestamos =  Prestamo::where('pagado', false)->get();
        $montos = $prestamos->sum('monto');
        $abonos = $prestamos->sum('abono');

        return $montos-$abonos;
    }
}
