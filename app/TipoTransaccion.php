<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TipoTransaccion
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Transaccion[] $transacciones
 * @mixin \Eloquent
 * @property int $id
 * @property string $nombre
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoTransaccion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoTransaccion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoTransaccion whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TipoTransaccion whereUpdatedAt($value)
 */
class TipoTransaccion extends Model
{

    /**
     * Atributos para assginacion masiva.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];


    /**
     * Retorna coleccion de Transacciones asociadas a un Tipo de Transaccion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transacciones()
    {
        return $this->hasMany(Transaccion::class);
    }

}
