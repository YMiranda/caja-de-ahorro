<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Silber\Bouncer\Database\HasRolesAndAbilities;

/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Ability[] $abilities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Abono[] $abonos
 * @property-read mixed $saldo_disponible
 * @property-read mixed $saldo_prestamos
 * @property-read mixed $saldo_retenido
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Prestamo[] $prestamos
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Transaccion[] $transacciones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIs($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsAll($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsNot($role)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{
    use Notifiable, HasRolesAndAbilities;

    /**
     * Atributos para que aceptan assginacion masiva.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'abono', 'fecha_abono'
    ];

    /**
     * Atributos protegidos que no se mostraran en las consultas.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Funcion para devolver una coleccion de Transacciones realizadas por un usuario
     * determinado.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transacciones()
    {
        return $this->hasMany(Transaccion::class, 'user_id');
    }

    /**
     * Funcion para devolver una coleccion de Prestamos realizadas por un usuario
     * determinado.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prestamos()
    {
        return $this->hasMany(Prestamo::class, 'user_id');
    }

    /**
     * Funcion para devolver una coleccion de Abonos realizadas por un usuario
     * determinado.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function abonos()
    {
        return $this->hasMany(Abono::class,'user_id');
    }

    /**
     * Mutador saldo_disponible retorna el Saldo Disponible el usuario como atributo del mismo
     * Requiere todas las trasacciones realizadas por el usuario para realizar el calculo usando
     * la siguiente formula:
     *
     * Saldo = Depositos-(Retiros+SaldoRetenido)
     * @return mixed Saldo Disponible del Usuario
     */
    public function getSaldoDisponibleAttribute()
    {
        $trans = $this->transacciones()->where('estado', false)->get();
        $retiros = $trans->where('tipo_transaccion_id', 2)->sum('monto');
        $depositos = $trans->where('tipo_transaccion_id', 1)->sum('monto');

        return $depositos-($retiros+$this->saldo_retenido);
    }

    /**
     * Mutador saldo_retenido retorna el Saldo Retenido el usuario como atributo del mismo
     * Se calcula de las Transacciones con el Identificador Retenido
     *
     * @return mixed Saldo Retenido del Usuario
     */
    public function getSaldoRetenidoAttribute()
    {
        $prestamos =  $this->prestamos()->where('pagado', false)->get();
        return $prestamos->sum('retenido');
    }

    /**
     * Mutador saldo_retenido retorna el Saldo Retenido el usuario como atributo del mismo
     * Se calcula restando los abonos a todos los montos prestados, solo considera los prestamos
     * que no se han terminado por completo.
     *
     * @return mixed Saldo de Prestamos que debe el Usuario
     */
    public function getSaldoPrestamosAttribute()
    {
        $prestamos =  $this->prestamos()->where('pagado', false)->get();
        $montos = $prestamos->sum('monto');
        $abonos = $prestamos->sum('abono');

        return $montos-$abonos;
    }
}
