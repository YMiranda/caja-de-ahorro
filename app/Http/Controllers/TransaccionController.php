<?php

namespace App\Http\Controllers;

use App\Abono;
use App\Prestamo;
use App\TipoTransaccion;
use App\Transaccion;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TransaccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Auth::user();
        if (Bouncer::is($usuario)->a('admin')){
            $transacciones = Transaccion::orderBy('id', 'desc')->get();
            $aportaciones = Abono::orderBy('id', 'desc')->get();
        }else{
            $transacciones = Transaccion::whereUserId($usuario->id)->orderBy('id', 'desc')->get();
            $aportaciones = Abono::whereUserId($usuario->id)->orderBy('id', 'desc')->get();
        }
        return view('resources.caja.historial')->with([
            'transacciones' => $transacciones,
            'aportaciones' => $aportaciones,
            'i' => 1
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deposito()
    {
        $pagar_prestamo = Prestamo::whereUserId(Auth::id())->where('pagado',false)->get()->last() ? true : false;
        return view('resources.caja.depostito')->with(['pagar_prestamo' => $pagar_prestamo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function depositar(Request $request)
    {
        Validator::make($request->all(), [
            'monto' => 'required|numeric',
            'prestamos' => 'nullable|boolean',
            'programar' => 'nullable|boolean',
            'fecha' => 'date|required_if:programar,true'
        ])->validate();

        DB::beginTransaction();

        $usuario = Auth::id();
        $monto = $request->monto;
        if ($request->has('prestamos'))
        {
            $prestamo = Prestamo::whereUserId($usuario)->where('pagado',false)->get()->last();
            $prestamo->abono += $monto;
            $prestamo->pagado = ($monto==$prestamo->abono) ? true : false;
            $prestamo->save();

            $tipoTransaccion = TipoTransaccion::whereNombre('Deposito')->first();
            $transacion = new Transaccion;
            $transacion->monto = $monto;
            $transacion->usuario()->associate(1);
            $transacion->tipo()->associate($tipoTransaccion);
            $transacion->save();

            if ($prestamo->pagado){
                $tipoTransaccion = TipoTransaccion::whereNombre('Liberado')->first();
                $transacion = new Transaccion;
                $transacion->monto = $prestamo->retenido;
                $transacion->usuario()->associate($usuario);
                $transacion->tipo()->associate($tipoTransaccion);
                $transacion->save();
            }
            $mensaje = 'Se realizo un pago de un prestamo por $' . $monto . ' a su cuenta con éxito!';
        }elseif ($request->has('programar'))
        {
            $abono = new Abono;
            $abono->monto = $monto;
            $abono->fecha_abono = Carbon::parse($request->fecha);
            $abono->usuario()->associate($usuario);
            $abono->save();

            $mensaje = 'Se programo un deposito $' . $monto . ' a su cuenta para el dia ' . $request->fecha;
        }else{
            $tipoTransaccion = TipoTransaccion::whereNombre('Deposito')->first();
            $transacion = new Transaccion;
            $transacion->monto = $monto;
            $transacion->usuario()->associate($usuario);
            $transacion->tipo()->associate($tipoTransaccion);
            $transacion->save();

            $mensaje = 'Se deposito $' . $monto . ' a su cuenta con éxito!';
        }
        DB::commit();
        return back()->with('message', $mensaje);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retiro()
    {
        $saldo = Auth::user()->saldo_disponible;
        if (!$saldo)
        {
            return redirect()->action('TransaccionController@index')->withErrors(['message' => 'No cuenta con fondos suficientes']);
        }
        return view('resources.caja.retiro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function retirar(Request $request)
    {
        Validator::make($request->all(), [
            'monto' => 'required|numeric',
        ])->validate();

        $tipoTransaccion = TipoTransaccion::whereNombre('Retiro')->first();
        $transacion = new Transaccion;
        $transacion->monto = $request->monto;
        $transacion->estado = ($request->monto > Auth::user()->saldo_disponible) ? true : false;
        $transacion->usuario()->associate(Auth::id());
        $transacion->tipo()->associate($tipoTransaccion);
        $transacion->save();

        if ($transacion->estado)
        {
            $mensaje = 'No cuenta con fondos suficientes';
        }
        else
        {
            $mensaje = 'Retiro de su cuenta realizado con éxito!';
        }
        return back()->with('message', $mensaje);
    }
}
