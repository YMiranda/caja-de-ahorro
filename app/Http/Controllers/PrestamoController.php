<?php

namespace App\Http\Controllers;

use App\Prestamo;
use App\Rules\CajaSinFondos;
use App\Rules\Garantia;
use App\TipoPrestamo;
use App\TipoTransaccion;
use App\Transaccion;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PrestamoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Auth::user();
        if (Bouncer::is($usuario)->a('admin')){
            $prestamos = Prestamo::with('tipo')->orderBy('id', 'desc')->get();
        }else{
            $prestamos = Prestamo::with('tipo')->whereUserId($usuario->id)->orderBy('id', 'desc')->get();
        }
        return view('resources.prestamos.historial')->with([
            'prestamos' => $prestamos,
            'i' => 1
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos_prestamos = TipoPrestamo::all();
        return view('resources.prestamos.nuevo')->with([
            'tipos_prestamos' => $tipos_prestamos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'monto' => ['required','numeric', new Garantia, new CajaSinFondos],
            'plazo' => 'required|exists:tipo_prestamos,id',
        ])->validate();

        DB::beginTransaction();

        $tipo_prestamo = TipoPrestamo::find($request->plazo);
        $monto = $request->monto;

        $interes = $tipo_prestamo->interes/100;
        $meses = $tipo_prestamo->meses;

        $monto_con_interes = $monto * (1+($interes*$meses));
        $retenido = ($monto)*0.2;

        $prestamo = new Prestamo;
        $prestamo->monto = $monto_con_interes;
        $prestamo->retenido = $retenido;
        $prestamo->tipo()->associate($tipo_prestamo);
        $prestamo->usuario()->associate(Auth::id());
        $prestamo->save();

        $tipoTransaccion = TipoTransaccion::whereNombre('Retenido')->first();
        $transacion = new Transaccion;
        $transacion->monto = $retenido;
        $transacion->usuario()->associate(Auth::id());
        $transacion->tipo()->associate($tipoTransaccion);
        $transacion->save();

        $tipoTransaccion = TipoTransaccion::whereNombre('Deposito')->first();
        $transacion = new Transaccion;
        $transacion->monto = $monto;
        $transacion->usuario()->associate(Auth::id());
        $transacion->tipo()->associate($tipoTransaccion);
        $transacion->save();

        DB::commit();

        return back()->with('message', 'Su prestamo por $' . $request->monto . '  fue depositado a su cuenta con exito!');
    }
}
