<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all()->whereNotIn('id',1);
        return view('resources.usuarios.historial')->with([
            'usuarios' => $usuarios
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->saldo_disponible > 0)
        {
            $mensaje = 'Error! Retire su saldo disponible antes de eliminar la cuenta.';
        }elseif ($user->saldo_prestamos > 0)
        {
            $mensaje = 'Error! Liquide todos sus Prestamos antes de eliminar la cuenta.';
        }else {
            $user->delete();
            $mensaje = 'Cuenta eliminnada correctamente.';
        }
        return back()->with('message', $mensaje);
    }
}
