<?php

namespace App\Http\Controllers;

use App\Transaccion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Bouncer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Auth::user();
        if (Bouncer::is($usuario)->a('admin')){
            $sd = Transaccion::saldoDisponible();
            $sp = Transaccion::saldoPrestado();
            $sr = Transaccion::saldoRetenido();
        }else{
            $sd = $usuario->saldo_disponible;
            $sp = $usuario->saldo_prestamos;
            $sr = $usuario->saldo_retenido;
        }
        return view('home')->with([
            'saldo_disponible' => $sd,
            'saldo_prestamos' => $sp,
            'saldo_retenido' => $sr
        ]);
    }
}
