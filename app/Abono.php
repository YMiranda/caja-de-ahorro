<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Abono
 *
 * @property-read \App\User $usuario
 * @mixin \Eloquent
 * @property int $id
 * @property float $monto
 * @property \Illuminate\Support\Carbon $fecha_abono
 * @property int $cobrado
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abono whereCobrado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abono whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abono whereFechaAbono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abono whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abono whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abono whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abono whereUserId($value)
 */
class Abono extends Model
{
    /**
     * Atributos para assginacion masiva.
     *
     * @var array
     */
    protected $fillable = [
        'monto', 'fecha_abono'
    ];

    /**
     * Crea instancias Carbon los siguientes atributos del modelo
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'fecha_abono'
    ];

    /**
     * Funcion retorna al Usuario asociado a el Abono.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
