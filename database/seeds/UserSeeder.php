<?php

use App\Prestamo;
use App\TipoTransaccion;
use App\Transaccion;
use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permisos para el Usuario 'Administrador'
        Bouncer::allow('sudo')->everything();
        $admin = Bouncer::allow('admin');

        $admin->to('listar', Transaccion::class);
        $admin->to('listar', Prestamo::class);
        $admin->to('listar', User::class);
        $admin->to('eliminar', User::class);

        // Permisos para el Usuario 'Cliente'
        $cliente = Bouncer::allow('cliente');
        $cliente->to('listar', Transaccion::class);
        $cliente->to('deposito', Transaccion::class);
        $cliente->to('retiro', Transaccion::class);
        $cliente->to('listar', Prestamo::class);
        $cliente->to('prestar', Prestamo::class);
        $cliente->to('eliminar', User::class);

        $admin = new User;
        $admin->name = 'Administrador de la Caja';
        $admin->email = 'admin@admin.com';
        $admin->password = Hash::make('admin');
        $admin->save();

        $admin->assign('admin');

        $usuario = new User;
        $usuario->name = 'Yair';
        $usuario->email = 'yair@yair.com';
        $usuario->password = Hash::make('yair');
        $usuario->save();

        $usuario->assign('cliente');

        $tipoTransaccion = TipoTransaccion::whereNombre('Deposito')->first();
        $transacion = new Transaccion;
        $transacion->monto = 10000;
        $transacion->usuario()->associate(1);
        $transacion->tipo()->associate($tipoTransaccion);
        $transacion->save();
    }
}
