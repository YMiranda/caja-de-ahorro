<?php

use App\{Prestamo, User};
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks=0");
        Prestamo::truncate();
        User::truncate();
        DB::statement("SET foreign_key_checks=1");

        $this->call([
            PrestamoSeeder::class,
            TipoTransacionSeeder::class,
            UserSeeder::class
        ]);
    }
}
