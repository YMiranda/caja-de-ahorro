<?php

use App\TipoTransaccion;
use Illuminate\Database\Seeder;

class TipoTransacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transacion = new TipoTransaccion();
        $transacion->nombre = 'Deposito';
        $transacion->save();

        $transacion = new TipoTransaccion();
        $transacion->nombre = 'Retiro';
        $transacion->save();

        $transacion = new TipoTransaccion();
        $transacion->nombre = 'Retenido';
        $transacion->save();

        $transacion = new TipoTransaccion();
        $transacion->nombre = 'Liberado';
        $transacion->save();
    }
}
