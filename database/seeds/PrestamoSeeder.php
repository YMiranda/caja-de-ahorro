<?php

use App\TipoPrestamo;
use Illuminate\Database\Seeder;

class PrestamoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prestamo = new TipoPrestamo;
        $prestamo->meses = 3;
        $prestamo->interes = 8.00;
        $prestamo->save();

        $prestamo = new TipoPrestamo;
        $prestamo->meses = 6;
        $prestamo->interes = 4.00;
        $prestamo->save();

        $prestamo = new TipoPrestamo;
        $prestamo->meses = 9;
        $prestamo->interes = 2.00;
        $prestamo->save();
    }
}
