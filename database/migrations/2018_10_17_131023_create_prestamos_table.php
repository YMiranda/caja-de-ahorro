<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto',10,2);
            $table->float('retenido',10,2);
            $table->float('abono',10,2)->default(0);
            $table->boolean('pagado')->default(false);
            $table->unsignedInteger('tipo_prestamo_id');
            $table->foreign('tipo_prestamo_id')->references('id')->on('tipo_prestamos');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prestamos', function (Blueprint $table) {
            $table->dropForeign(['tipo_prestamo_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('prestamos');
    }
}
