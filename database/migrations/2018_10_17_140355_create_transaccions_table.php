<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaccions', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto',12,2);
            $table->unsignedInteger('tipo_transaccion_id');
            $table->foreign('tipo_transaccion_id')->references('id')->on('tipo_transaccions');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->boolean('estado')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaccions', function (Blueprint $table) {
            $table->dropForeign(['tipo_transaccion_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('transaccions');
    }
}
